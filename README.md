### ReflON

ReflON is a work-in-progress data interchange format inspired by JSON, but with
a major difference: ReflON documents can give a value as a reference (denoted
by `$`) to another part of the document rather than as data, in order to avoid
duplicating data.

For example:

```
{
	hello: ["World"],
	other: {
		data: $../../hello/[0]
	}
}
```

Here `data` contains a reference that points to the first element of the list
held by `hello` in the parent object of `other`. Calling `resolve_references()`
on this ReflON would result in:

```
{
	hello: ["World"],
	other: {
		data: "World"
	}
}
```

Had `$../../hello/[0]` held a large volume of data, the un-expanded ReflON
would have saved a significant amount of memory. This format can also be used
to encode infinite data structures (although these need to be traversed
manually and calling `resolve_references()` will fail since it eagerly
evaluates the entire document):

```
{
	elem: 0x4c,
	next: $..
}
```

This is effectively an infinite object tower with each level containing an
`elem` attribute with value `0x4c`.

### Important Notes

ReflON is intended more as a proof-of-concept than a real attempt to replace
JSON; this library is not thoroughly tested (and contains serious bugs at the
time of writing), and more importantly it is not backwards-compatible with JSON
(which would obviously be a requirement for any successor to achieve sizeable
adoption due to the present ubiquity of JSON). In particular, field names are
not quoted unlike in JSON, and `null` is not supported. Additional differences
include that numbers can also be given in binary, octal or hexadecimal, and
numbers can include `_`s as visual separators; additionally floats and integers
are treated as separate data types. There are also some small changes to escape
sequences, such as allowing `\u{...}` to include a variable number of Unicode
bytes.
