extern crate pom;

pub mod ast;
pub mod deref;
pub mod parse;

pub use ast::*;
pub use deref::{NotFoundErr, ResolutionErr};
pub use parse::UCDErr;
use parse::{reflon_doc, RP};
use pom::Result;

pub(crate) fn parse_v<'a, 'b, T, F>(v: &'b Vec<char>, f: F) -> Result<T>
where 'b: 'a, F: Fn() -> RP<'a, T> {
	f().parse(&v)
}

pub(crate) fn char_vec(s: &str) -> Vec<char> {
	s.chars().collect()
}

pub fn parse(s: &str) -> Result<Reflon> {
	parse_v(&char_vec(s), reflon_doc)
}

#[cfg(test)]
mod test {
	use crate::*;

	#[test]
	fn basic_test() {
		let mut ast = parse("{a: [\"b\"], other: $../../a/[0]}").unwrap();
		ast.resolve_references().unwrap()
	}
}
