use crate::ast::*;
use std::collections::HashMap;
use std::error::Error;
use std::fmt::{self, Display, Formatter};
use std::iter::FromIterator;
use std::str::FromStr;
use pom::char_class::alpha;
use pom::parser::{self, *};

pub(crate) type RP<'a, O> = parser::Parser<'a, char, O>;

#[derive(Debug)]
pub enum UCDErr {
	/// Bytes given in a `\u{...}` escape sequence did not represent a valid
	/// Unicode character
	Invalid
}

impl Display for UCDErr {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		match *self {
			Self::Invalid => {
				let msg = "given bytes do not represent a valid Unicode character";
				write!(f, "{}", msg)
			}
		}
	}
}

impl Error for UCDErr {}

fn skip_all<'a, T: 'a>(p: RP<'a, T>) -> RP<'a, ()> {
	p.repeat(0 ..).discard()
}

// https://www.unicode.org/Public/UCD/latest/ucd/PropList.txt
fn wspace<'a>() -> RP<'a, ()> {
	let ascii_wspace = one_of(" \n\r\t\x0B\x0C");
	let eq_to_hs = |c: char| c as u32 >= 0x2000 && c as u32 <= 0x200A;
	let misc0 = one_of("\u{0085}\u{00A0}\u{1680}\u{2028}");
	let misc1 = one_of("\u{2029}\u{202F}\u{205F}\u{3000}");
	(ascii_wspace | misc0 | misc1 | is_a(eq_to_hs)).discard()
}

fn comment<'a>() -> RP<'a, ()> {
	let single = tag("//") * skip_all(none_of("\n")) - sym('\n');
	let not = none_of("*") | sym('*') * none_of("/");
	let multi = tag("/*") * skip_all(not) - tag("*/");
	single | multi
}

// Any stuff that should be accepted but ignored
fn ignored<'a>() -> RP<'a, ()> {
	skip_all(wspace() | comment())
}

fn dec_digit<'a>() -> RP<'a, char> {
	one_of("0123456789")
}

// A string of digits of any length, permitting `_` as a separator
fn digits<'a, F: Fn() -> RP<'a, char>>(digit: F) -> RP<'a, String> {
	let single = digit() - sym('_').repeat(..);
	let many = digit() + single.repeat(..) + digit();
	(digit().collect() | many.collect()).map(String::from_iter)
}

fn dec_digits<'a>() -> RP<'a, String> {
	digits(dec_digit)
}

fn hex_digit<'a>() -> RP<'a, char> {
	one_of("0123456789abcdefABCDEF")
}

// Accept the string `s` and return the value `t`
fn value<'a, 'b, T: 'a + Copy>(s: &'b str, t: T) -> RP<'a, T>
where 'b: 'a {
	tag(s).map(move |_| t)
}

fn boolean<'a>() -> RP<'a, bool> {
	value("true", true) | value("false", false)
}

// Accept an integer, which may be decimal, hex, binary, or octal
fn int<'a>() -> RP<'a, i64> {
	let hex = value("0x", 16) + digits(hex_digit);
	let bin = value("0b", 2) + digits(|| one_of("01"));
	let oct = value("0o", 8) + digits(|| one_of("01234567"));
	let dec = empty().map(|_| 10) + dec_digits();
	let sign = value("-", -1) | empty().map(|_| 1);
	let num = sign + (hex | bin | oct | dec);
	let combine = |t: (i64, (u32, String))| {
		i64::from_str_radix(&t.1.1, t.1.0).map(|i| t.0 * i)
	};
	num.convert(combine)
}

fn float<'a>() -> RP<'a, f64> {
	let fract = sym('.') + dec_digits();
	let exp = one_of("eE") + one_of("+-").opt() + dec_digits();
	let num = sym('-').opt() + dec_digits() + fract + exp.opt();
	num.collect().convert(|s| f64::from_str(&String::from_iter(s)))
}

// Accept up to 8 hex digits representing a Unicode character
fn ucd_code(cs: Vec<char>) -> Result<char, UCDErr> {
	// All chars received at this point must be valid hex digits
	let digit = |c: char| c.to_digit(16).unwrap() as u8;
	let shift = |v: (usize, u8)| (v.1 as u32) << (1 << (v.0 * 4));
	let u = cs.into_iter().map(digit).enumerate().map(shift).sum();
	char::from_u32(u).ok_or(UCDErr::Invalid)
}

// Accept a string with standard escapes and support for `\u{...}` syntax
fn string<'a>() -> RP<'a, String> {
	let ascii_ws = value("\\n", '\n') | value("\\r", '\r') | value("\\t", '\t');
	let ascii_o = value("\\\\", '\\') | value("\\0", '\0') | value("\\\"", '"');
	let ucd_digits = hex_digit().repeat(1 .. 9).convert(ucd_code);
	let ucd = tag("\\u{") * ucd_digits - sym('}');
	let all = (ascii_ws | ascii_o | ucd | none_of("\0\"")) - tag("\\n");
	sym('"') * all.repeat(0 ..).collect().map(String::from_iter) - sym('"')
}

fn alpha_char(c: char) -> bool {
	c.is_ascii() && alpha(c as u8)
}

fn identifier<'a>() -> RP<'a, String> {
	let bodyelem = is_a(|c: char| c.is_ascii_alphanumeric());
	let body = bodyelem.repeat(0 ..);
	(is_a(alpha_char) + body).collect().map(String::from_iter)
}

fn sep<'a>() -> RP<'a, ()> {
	sym(',') * ignored()
}

fn list_of<'a, T: 'a>(enc: (char, char), elem: RP<'a, T>) -> RP<'a, Vec<T>> {
	let elems = list(elem, sep());
	sym(enc.0) * ignored() * elems - sep().opt() - sym(enc.1)
}

fn object<'a>() -> RP<'a, HashMap<String, Reflon>> {
	let member = identifier() - ignored() - sym(':') - ignored() + call(reflon);
	let obj = list_of(('{', '}'), member);
	obj.map(|members| members.into_iter().collect::<HashMap<_, _>>())
}

fn array<'a>() -> RP<'a, Vec<Reflon>> {
	list_of(('[', ']'), call(reflon))
}

// Accept a path that references an element within a ReflON document
fn path<'a>() -> RP<'a, RefPath> {
	let parent = tag("..").map(|_| PathElem::Parent);
	let id = identifier().map(PathElem::Elem);
	let idxdigits = dec_digits().convert(|s: String| i128::from_str(&s));
	let combine = |t: (Option<_>, i128)| {
		if t.0.is_some() {
			-t.1
		}
		else {
			t.1
		}
	};
	let idxraw = sym('[') * (sym('-').opt() + idxdigits).map(combine) - sym(']');
	let idx = idxraw.map(PathElem::Index);
	let p = sym('/').opt() + list(parent | id | idx, sym('/'));
	p.map(|t| RefPath::new(t.0.is_some(), t.1))
}

fn reference<'a>() -> RP<'a, RefPath> {
	sym('$') * path()
}

fn reflon<'a>() -> RP<'a, Reflon> {
	let p0 = boolean().map(Reflon::Bool) | int().map(Reflon::Int);
	let p1 = float().map(Reflon::Float) | string().map(Reflon::Str);
	let adv = object().map(Reflon::Obj) | array().map(Reflon::List);
	(p0 | p1 | adv | reference().map(Reflon::Ref)) - ignored()
}

pub(crate) fn reflon_doc<'a>() -> RP<'a, Reflon> {
	ignored() * reflon() - end()
}

#[cfg(test)]
mod test {
	use super::*;
	use crate::*;

	#[test]
	fn ref_test() {
		let s = "../../[-1]/a/b1";
		let out = parse_v(&char_vec(s), path).unwrap();
		assert_eq!(format!("{}", out), s)
	}

	#[test]
	fn id_test() {
		let f = |s: &str| parse_v(&char_vec(s), identifier);
		let s = "hell1o2";
		assert_eq!(f(s).unwrap(), s);
		let bad0 = "12";
		assert!(f(bad0).is_err());
		let bad1 = "[";
		assert!(f(bad1).is_err())
	}

	#[test]
	fn obj_test() {
		let s = "{hello: \"World\"}";
		let out = parse_v(&char_vec(s), object).unwrap();
		let elem = out.get("hello").unwrap();
		if let Reflon::Str(s) = elem {
			assert_eq!(s, "World")
		}
		else {
			panic!("not a string")
		}
	}

	#[test]
	fn array_test() {
		let s = "[1, 1, 1]";
		let out = parse_v(&char_vec(s), array).unwrap();
		for r in out {
			if let Reflon::Int(i) = r {
				assert_eq!(i, 1)
			}
			else {
				panic!("not an int")
			}
		}
	}
}
