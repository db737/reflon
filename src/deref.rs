use crate::ast::*;
use std::convert::TryFrom;
use std::error::Error;
use std::fmt::{self, Display, Formatter};
use std::mem;

/*
 * TODO recursion counters on relevant functions to handle overflow gracefully?
 */

#[derive(Debug)]
pub struct NotFoundErr(pub String);

impl NotFoundErr {
	pub fn empty() -> Self {
		Self(String::from(""))
	}

	pub fn new(msg: &str) -> Self {
		Self(String::from(msg))
	}
}

impl Display for NotFoundErr {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		write!(f, "{}", self.0)
	}
}

impl Error for NotFoundErr {}

#[derive(Debug)]
pub enum ResolutionErr {
	/// Resolving the reference(s) failed due to there being a reference cycle,
	/// for example if `a` references `b` but `b` references back to `a`.
	CyclicDependency,
	/// The reference could not be resolved as it does not point to an element
	/// of the document that exists.
	NotFound(RefPath, NotFoundErr)
}

impl Display for ResolutionErr {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		match self {
			Self::CyclicDependency => write!(f, "cyclic reference found"),
			Self::NotFound(p, e) => {
				write!(f, "reference {} could not be resolved: {}", p, e)
			}
		}
	}
}

impl Error for ResolutionErr {}

enum FindOutcome {
	AnotherRef,
	Found(Reflon),
	NotFound(NotFoundErr)
}

enum StepOutcome {
	Find(FindOutcome),
	Parent
}

impl StepOutcome {
	pub fn not_found(e: NotFoundErr) -> Self {
		Self::Find(FindOutcome::NotFound(e))
	}
}

fn get<T: Clone>(v: &Vec<T>, i: i128) -> Result<T, NotFoundErr> {
	let l = v.len();
	let msg0 = format!("list index {} out of range for list of length {}", i, l);
	let e0 = NotFoundErr(msg0);
	let s = mem::size_of::<usize>();
	let msg1 = format!("list index {} is too big for the pointer size {}", i, s);
	let e1 = NotFoundErr(msg1);
	if i >= 0 {
		if i >= l as i128 {
			Err(e0)
		}
		else {
			let idx = usize::try_from(i).map_err(|_| e1)?;
			Ok(v[idx].clone())
		}
	}
	else {
		let imr = l as i128 + i;
		if imr < 0 {
			Err(e0)
		}
		else {
			let im = usize::try_from(imr).map_err(|_| e1)?;
			Ok(v[im].clone())
		}
	}
}

fn locate_step(path: &RefPath, ast: &Reflon) -> StepOutcome {
	let mut path0 = path.clone();
	match path0.take_head() {
		Some(PathElem::Parent) => StepOutcome::Parent,
		Some(PathElem::Index(i)) => match ast {
			Reflon::List(v) => match get(v, i) {
				Ok(r) => match locate_step(path, &r) {
					StepOutcome::Parent => {
						let mut path1 = path0.clone();
						path1.take_head().unwrap();
						locate_step(&path1, ast)
					},
					so => so
				},
				Err(e) => StepOutcome::not_found(e)
			},
			Reflon::Ref(_) => {
				StepOutcome::Find(FindOutcome::AnotherRef)
			}
			_ => {
				let e = NotFoundErr::new("cannot index into non-array element");
				StepOutcome::not_found(e)
			}
		},
		Some(PathElem::Elem(s)) => {
			let e = NotFoundErr(format!("element {} not found", s));
			let so0 = StepOutcome::not_found(e);
			match ast {
				Reflon::Obj(hm) => match hm.get(&s) {
					Some(r) => match locate_step(&path0, r) {
						StepOutcome::Parent => {
							let mut path1 = path0.clone();
							path1.take_head().unwrap();
							locate_step(&path1, ast)
						},
						so1 => so1
					}
					None => so0
				},
				Reflon::Ref(_) => {
					StepOutcome::Find(FindOutcome::AnotherRef)
				},
				_ => so0
			}
		},
		None => StepOutcome::Find(FindOutcome::Found(ast.clone()))
	}
}

fn locate(r: &RefPath, ast: &Reflon) -> FindOutcome {
	match locate_step(r, ast) {
		StepOutcome::Parent => {
			let msg = "cannot reference the parent (..) of the root object";
			FindOutcome::NotFound(NotFoundErr::new(msg))
		},
		StepOutcome::Find(fo) => fo
	}
}

#[derive(Clone, Copy)]
pub(crate) struct AstState {
	// Whether the ReflON has been updated in this step
	pub changed: bool,
	// Whether there are still unresolved references that weren't updated
	// due to referencing another reference transitively
	pub refs_remain: bool
}

impl AstState {
	pub fn compose(self, other: Self) -> Self {
		Self {
			changed: self.changed || other.changed,
			refs_remain: self.refs_remain || other.refs_remain
		}
	}

	pub fn new(changed: bool, refs_remain: bool) -> Self {
		Self {
			changed,
			refs_remain
		}
	}
}

type Res0 = Result<AstState, ResolutionErr>;

fn first_err(r0: Res0, r1: Res0) -> Res0 {
	match (r0, r1) {
		(Ok(b0), Ok(b1)) => Ok(b0.compose(b1)),
		(Ok(_), Err(e)) => Err(e),
		(Err(e), Ok(_)) => Err(e),
		(Err(e0), Err(_)) => Err(e0)
	}
}

pub(crate) fn res_ref_step(r: &RefPath, ast: &mut Reflon, full: &Reflon) -> Res0 {
	let mut path = r.clone();
	let mut new = None;
	let out = match ast {
		Reflon::List(v) => {
			let mut res = Ok(AstState::new(false, false));
			// Visit each element, keeping track of the path to it
			for (i, r) in v.iter_mut().enumerate() {
				path.push(PathElem::Index(i as i128));
				res = first_err(res, res_ref_step(&path, r, full));
				path.strip()
			};
			res
		},
		Reflon::Obj(hm) => {
			let mut res = Ok(AstState::new(false, false));
			// Visit each element, keeping track of the path to it
			for (k, v) in hm {
				path.push(PathElem::Elem(k.clone()));
				res = first_err(res, res_ref_step(&path, v, full));
				path.strip()
			};
			res
		}
		Reflon::Ref(r) => {
			let mut full_path = r.clone();
			// Transform a relative path into an absolute one
			if !r.root {
				full_path.prepend_all(path.body)
			};
			// Try to resolve the reference
			match locate(&full_path, full) {
				FindOutcome::AnotherRef => Ok(AstState::new(false, true)),
				FindOutcome::Found(reflon) => {
					new = Some(reflon);
					Ok(AstState::new(true, false))
				},
				FindOutcome::NotFound(e) => {
					Err(ResolutionErr::NotFound(r.clone(), e))
				}
			}
		},
		_ => Ok(AstState::new(false, false))
	};
	if let Some(reflon) = new {
		*ast = reflon
	};
	out
}
