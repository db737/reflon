use crate::ResolutionErr;
use crate::deref::{res_ref_step, AstState};
use std::collections::HashMap;
use std::fmt::{self, Display, Formatter};

#[derive(Clone, Debug)]
pub enum PathElem {
	/// An element of an object
	Elem(String),
	/// An index of an array
	Index(i128),
	/// The parent object
	Parent
}

impl Display for PathElem {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		match self {
			Self::Elem(s) => write!(f, "{}", s),
			Self::Index(i) => write!(f, "[{}]", i),
			Self::Parent => write!(f, "..")
		}
	}
}

/// A data structure that represents a parsed ReflON. It may contain unresolved
/// references.
#[derive(Clone, Debug)]
pub enum Reflon {
	/// A boolean value (`true` or `false`)
	Bool(bool),
	/// A floating point number
	Float(f64),
	/// A signed integer
	Int(i64),
	/// A list of elements (which may be of differing types)
	List(Vec<Reflon>),
	/// An object, i.e. a collection of identifiers with their associated values
	Obj(HashMap<String, Reflon>),
	/// A reference to another part of this document (which may or may not be
	/// valid)
	Ref(RefPath),
	/// A quoted string
	Str(String)
}

impl Reflon {
	/// Resolve all the references in the given ReflON document, including
	/// transitive ones. If there is a reference cycle that makes this impossible,
	/// `ResolutionErr::CyclicDependency` is returned. If the references lead to
	/// infinite regress this function will panic with a stack overflow
	pub fn resolve_references(&mut self) -> Result<(), ResolutionErr> {
		let path = RefPath::root();
		let mut ast_state = AstState::new(false, false);
		while !ast_state.changed {
			match res_ref_step(&path, self, &self.clone()) {
				Ok(new_state) => ast_state = new_state,
				Err(e) => return Err(e)
			}
		};
		if ast_state.refs_remain {
			Err(ResolutionErr::CyclicDependency)
		}
		else {
			Ok(())
		}
	}

	/// Whether this ReflON document contains any references.
	pub fn contains_references(&self) -> bool {
		match self {
			Reflon::List(v) => v.iter().any(Self::contains_references),
			Reflon::Obj(hm) => hm.values().any(Self::contains_references),
			Reflon::Ref(_) => true,
			_ => false
		}
	}
}

/// A reference to an element in a ReflON document. `root` indicates that this
/// is an absolute reference, i.e. one that begins at the root of the document.
#[derive(Clone, Debug)]
pub struct RefPath {
	pub body: Vec<PathElem>,
	pub root: bool
}

impl RefPath {
	pub fn new(root: bool, body: Vec<PathElem>) -> Self {
		Self {
			root,
			body
		}
	}

	pub fn root() -> Self {
		Self {
			root: true,
			body: Vec::new()
		}
	}

	pub fn push(&mut self, p: PathElem) {
		self.body.push(p)
	}

	pub fn strip(&mut self) {
		let l = self.body.len();
		if l > 0 {
			self.body.remove(self.body.len() - 1);
		}
	}

	pub fn prepend(&mut self, prefix: PathElem) {
		self.prepend_all(vec![prefix])
	}

	pub fn prepend_all(&mut self, prefix: Vec<PathElem>) {
		let mut new = prefix.clone();
		new.append(&mut self.body);
		self.body = prefix
	}

	pub fn take_head(&mut self) -> Option<PathElem> {
		self.root = false;
		if self.body.len() == 0 {
			None
		}
		else {
			Some(self.body.remove(0))
		}
	}
}

impl Display for RefPath {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		if self.root {
			write!(f, "/")?
		};
		let l = self.body.len();
		if l == 0 {
			Ok(())
		}
		else {
			for i in 0 .. l - 1 {
				write!(f, "{}", self.body[i])?;
				write!(f, "/")?
			};
			write!(f, "{}", self.body[l - 1])
		}
	}
}
